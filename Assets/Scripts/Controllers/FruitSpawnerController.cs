using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FruitNinja.ObjectPool;
using Random = UnityEngine.Random;
using FruitNinja.Fruits;

namespace FruitNinja.Controllers
{
    public class FruitSpawnerController : MonoBehaviour, IFruitSpawner
    {
        [SerializeField] Pooler fruitPooler;
        [SerializeField] private float spawnIntervalMin = 0.5f;
        [SerializeField] private float spawnIntervalMax = 1f;
        [SerializeField] private float spawnPositionXMin;
        [SerializeField] private float spawnPositionXMax;
        [SerializeField] private float spawnDirectionXMin;
        [SerializeField] private float spawnDirectionXMax;

        private const float SpawnPositionZ = 0f;
        private const float SpawnPositionY = -5f;
        private const float SpawnDirectionY = 8f;
        private const float LaunchForceMin = 12f;
        private const float LaunchForceMax = 13f;
        private const int RottenFruitSpawnChance = 50;

        private Coroutine spawnCoroutine;

        public event Action<ISpawnableFruit> FruitSliced;
        public event Action<ISpawnableFruit> FruitHitFloor;

        public void StartSpawner()
        {
            spawnCoroutine = StartCoroutine(SpawnCoroutine());
        }

        private IEnumerator SpawnCoroutine()
        {
            while (true)
            {
                Vector3 spawnPosition = new(Random.Range(spawnPositionXMin, spawnPositionXMax), SpawnPositionY, SpawnPositionZ);
                Vector3 launchPosition = new(Random.Range(spawnDirectionXMin, spawnDirectionXMax), SpawnDirectionY, SpawnPositionZ);

                Vector3 direction = launchPosition - spawnPosition;
                direction.z = 0f;
                direction.Normalize();

                SpawnableFruit spawnableFruit;
                
                int chance = Random.Range(0, 101);
                
                if (chance > 100 - RottenFruitSpawnChance)
                {
                    spawnableFruit = fruitPooler.GetPooledObjectOfType<Fruit>();
                }
                else
                {
                    spawnableFruit = fruitPooler.GetPooledObjectOfType<RottenFruit>();
                }

                spawnableFruit.transform.position = spawnPosition;
                spawnableFruit.SetSpawner(this);
                spawnableFruit.gameObject.SetActive(true);

                spawnableFruit.Launch(direction, Random.Range(LaunchForceMin, LaunchForceMax));

                yield return new WaitForSeconds(Random.Range(spawnIntervalMin, spawnIntervalMax));
            }
        }

        public void StopSpawner()
        {
            StopCoroutine(spawnCoroutine);
        }

        public SlicedFruit SpawnSlicedFruit(Vector3 position, Quaternion rotation)
        {
            SlicedFruit sliced = fruitPooler.GetPooledObjectOfType<SlicedFruit>();
            sliced.transform.position = position;
            sliced.transform.rotation = rotation;

            sliced.gameObject.SetActive(true);

            return sliced;
        }

        public void DespawnAllFruits()
        {
            fruitPooler.ReclaimAllPooledObject();
        }

        public void OnFruitSliced(ISpawnableFruit fruit)
        {
            FruitSliced?.Invoke(fruit);
        }

        public void OnFruitHitFloor(ISpawnableFruit fruit)
        {
            FruitHitFloor?.Invoke(fruit);
        }

        public void Despawn(ISpawnableFruit fruit)
        {
            fruitPooler.ReclaimPooledObject(fruit as Poolable);
        }

        ISpawnableFruit IFruitSpawner.Spawn<T>()
        {
            return null;
        }
    }
}

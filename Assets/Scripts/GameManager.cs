using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FruitNinja.Controllers;
using System;
using FruitNinja.UI;
using FruitNinja.Fruits;

namespace FruitNinja.Game
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private int playTime = 10;
        [SerializeField] private FruitSpawnerController fruitSpawner;
        [SerializeField] private Slicer slicer;
        [SerializeField] private StartMenuController startMenuController;
        [SerializeField] private PausePopupController pausePopupController;
        [SerializeField] private GameOverPopupController gameOverPopupController;

        private const int DefaultHealth = 5;

        private int sliced, missed, health;
        private float timer;
        private bool isPaused, isInGame;
        public event Action<int> FruitSliced, FruitMissed, HealthChanged;
        public event Action<float> TimeChanged;

        public int Sliced
        {
            get => sliced;
            set
            {
                sliced = value;
                FruitSliced?.Invoke(sliced);
            }
        }

        public int Missed
        {
            get => missed;
            set
            {
                missed = value;
                FruitMissed?.Invoke(missed);
            }
        }

        public int Health
        {
            get => health;
            set
            {
                health = Mathf.Max(0, value);
                HealthChanged?.Invoke(health);

                if (health <= 0)
                {
                    GameOver();
                }
            }
        }

        public float Timer
        {
            get => timer;
            set
            {
                timer = Mathf.Max(0f, value);
                TimeChanged?.Invoke(timer);
            }
        }

        private void Awake() 
        {
            startMenuController.PlayPressed += OnPlayPressed;

            pausePopupController.ContinuePressed += OnContinuePressed;
            pausePopupController.RestartPressed += OnRestartPressed;
            pausePopupController.QuitPressed += OnQuitPressed;

            gameOverPopupController.PlayAgainPressed += OnRestartPressed;
            gameOverPopupController.QuitPressed += OnQuitPressed;

            fruitSpawner.FruitSliced += OnFruitSliced;
            fruitSpawner.FruitHitFloor += OnFruitHitFloor;
        }

        private void Update() 
        {
            if (!isInGame) return;

            if (!isPaused && Input.GetKeyDown(KeyCode.P))
            {
                Pause();
            }

            if (Timer > 0f)
            {
                Timer -= Time.deltaTime;
                if (Timer <= 0f)
                {
                    GameOver();
                }
            }
        }

        private void OnDestroy() 
        {
            startMenuController.PlayPressed -= OnPlayPressed;

            pausePopupController.ContinuePressed -= OnContinuePressed;
            pausePopupController.RestartPressed -= OnRestartPressed;
            pausePopupController.QuitPressed -= OnQuitPressed;

            gameOverPopupController.PlayAgainPressed -= OnRestartPressed;
            gameOverPopupController.QuitPressed -= OnQuitPressed;

            fruitSpawner.FruitSliced -= OnFruitSliced;
            fruitSpawner.FruitHitFloor -= OnFruitHitFloor;
        }

        private void StartGame()
        {
            Reset();
            isInGame = true;
            fruitSpawner.StartSpawner();
            slicer.Activate();
        }

        private void Pause()
        {
            isPaused = !isPaused;

            if (isPaused)
            {
                pausePopupController.Show();
                Time.timeScale = 0f;
                slicer.Deactivate();
            }
            else
            {
                pausePopupController.Hide();
                Time.timeScale = 1f;
                slicer.Activate();
            }
        }

        private void Reset()
        {
            Sliced = 0;
            Missed = 0;
            Timer = playTime;
            Health = DefaultHealth;

            pausePopupController.Hide();
            gameOverPopupController.Hide();
            startMenuController.Hide();
            isPaused = false;
            
            Time.timeScale = 1f;
        }

        private void GameOver()
        {
            slicer.Deactivate();
            isInGame = false;
            fruitSpawner.StopSpawner();
            gameOverPopupController.Show();
        }

        private void Quit()
        {
            Reset();
            isInGame = false;
            fruitSpawner.StopSpawner();
            fruitSpawner.DespawnAllFruits();
            
            startMenuController.Show();
        }

        private void OnPlayPressed()
        {
            StartGame();
        }

        private void OnContinuePressed()
        {
            Pause();
        }

        private void OnRestartPressed()
        {
            slicer.Deactivate();
            fruitSpawner.DespawnAllFruits();
            StartGame();
        }

        private void OnQuitPressed()
        {
            Quit();
        }

        private void OnFruitSliced(ISpawnableFruit spawnableFruit)
        {
            if (!isInGame) return;

            if (spawnableFruit is Fruit)
            {
                SliceFruit(spawnableFruit);
            }
            else if (spawnableFruit is RottenFruit)
            {
                SliceRottenFruit(spawnableFruit as RottenFruit);
            }
        }

        private void SliceRottenFruit(RottenFruit fruit)
        {
            fruitSpawner.Despawn(fruit);
            Health--;
        }

        private void SliceFruit(ISpawnableFruit spawnableFruit)
        {
            Fruit fruit = spawnableFruit as Fruit;
            SlicedFruit sliced = fruitSpawner.SpawnSlicedFruit(fruit.transform.position, fruit.transform.rotation);
            sliced.Slice(fruit.GetVelocity() * 0.75f, 0.5f);

            fruitSpawner.Despawn(fruit);
            Sliced++;
        }

        private void OnFruitHitFloor(ISpawnableFruit spawnableFruit)
        {
            fruitSpawner.Despawn(spawnableFruit);
            if (!isInGame) return;
            if (spawnableFruit is Fruit) Missed++;
        }
    }
}

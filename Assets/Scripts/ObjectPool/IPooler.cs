namespace FruitNinja.ObjectPool
{
    public interface IPooler<T> where T : IPoolable<T>
    {
        void InitPool();
        void ResetPool();
        IPoolable<T> GetPooledObject();
        IPoolable<T> InstantiateObject();
        void ReclaimPooledObject(IPoolable<T> obj);
        void ReclaimAllPooledObject();
    }
}
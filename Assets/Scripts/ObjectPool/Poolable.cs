using System;
using UnityEngine;

namespace FruitNinja.ObjectPool
{
    public abstract class Poolable : MonoBehaviour, IPoolable<Poolable>
    {
        protected Pooler pooler;
        private bool canBeUsed;
        public virtual bool CanBeUsed()
        {
            return !gameObject.activeInHierarchy && canBeUsed;
        }
        public virtual void Init(IPooler<Poolable> pooler)
        {
            this.pooler = (Pooler) pooler;
            SetCanBeUsed(true);
        }

        public virtual void OnReclaim()
        {
            
        }

        public void SelfReclaim()
        {
            pooler.ReclaimPooledObject(this);
        }

        public void SetCanBeUsed(bool canBeUsed)
        {
            this.canBeUsed = canBeUsed;
        }
    }

    [Serializable]
    public class PoolablePrefab
    {
        public Poolable prefab;
        public int instantiateCount;
        private int index;

        public int Index { get => index; private set => index = value; }

        public void IncrementIndex() => Index++;
    }
}
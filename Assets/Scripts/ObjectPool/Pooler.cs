using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja.ObjectPool
{
    public class Pooler : MonoBehaviour, IPooler<Poolable>
    {
        [SerializeField] PoolablePrefab[] prefabs;
        private readonly List<Poolable> poolables = new List<Poolable>();

        protected Poolable CurrentPrefab {get; private set;}

        private void Awake() 
        {
            InitPool();
        }
        public void InitPool()
        {
            if (prefabs.Length == 0) return;
            foreach (PoolablePrefab poolablePrefab in prefabs)
            {
                CurrentPrefab = poolablePrefab.prefab;
                for (int i = 0; i < poolablePrefab.instantiateCount; i++)
                {
                    Poolable poolable = InstantiateObject() as Poolable;
                    poolable.SetCanBeUsed(true);
                    poolables.Add(poolable);
                }
            }
        }

        public T GetPooledObjectOfType<T>() where T : Poolable
        {
            foreach (Poolable poolable in poolables)
            {
                if (poolable is T && poolable.CanBeUsed())
                {
                    poolable.SetCanBeUsed(false);
                    return poolable as T;
                }
            }

            return InstantiateObject<T>() as T;
        }

        private IPoolable<Poolable> InstantiateObject<T>()
        {
            if (CurrentPrefab == null || CurrentPrefab is not T)
            {
                foreach (PoolablePrefab poolablePrefab in prefabs)
                {
                    if (poolablePrefab.prefab is T)
                    {
                        CurrentPrefab = poolablePrefab.prefab;
                    }
                }
            }

            return InstantiateObject();
        }
        
        public IPoolable<Poolable> GetPooledObject()
        {
            foreach (Poolable poolable in poolables)
            {
                if (poolable.CanBeUsed())
                {
                    poolable.SetCanBeUsed(false);
                    return poolable;
                }
            }

            return InstantiateObject();
        }

        public IPoolable<Poolable> InstantiateObject()
        {
            if (CurrentPrefab == null) return null;

            Poolable poolable = Instantiate(CurrentPrefab, transform).GetComponent<Poolable>();
            poolable.gameObject.SetActive(false);
            poolable.Init(this);
            poolable.SetCanBeUsed(false);

            return poolable;
        }

        public void ReclaimAllPooledObject()
        {
            foreach (Poolable poolable in poolables)
            {
                ReclaimPooledObject(poolable);
            }
        }

        public void ReclaimPooledObject(IPoolable<Poolable> obj)
        {
            Poolable poolable = obj as Poolable;

            poolable.gameObject.SetActive(false);
            poolable.transform.SetParent(transform);
            poolable.transform.position = Vector3.zero;
            poolable.transform.localPosition = Vector3.zero;

            poolable.OnReclaim();
            poolable.SetCanBeUsed(true);
        }

        public void ResetPool()
        {
            ReclaimAllPooledObject();
        }
    }
}
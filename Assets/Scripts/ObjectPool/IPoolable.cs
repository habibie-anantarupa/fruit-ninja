namespace FruitNinja.ObjectPool
{
    public interface IPoolable<T> where T : IPoolable<T>
    {
        void Init(IPooler<T> pooler);
        void SelfReclaim();
        void OnReclaim();
        bool CanBeUsed();
        void SetCanBeUsed(bool canBeUsed);
    }
}
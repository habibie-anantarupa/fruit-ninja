using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja.Game
{
    public class InputController : MonoBehaviour
    {
        public event Action MouseDown, MouseUp;
        private Vector3 mousePosition;
        private float cameraNearClip;
        private Camera mainCamera;

        private void Awake() 
        {
            mainCamera = Camera.main;
            cameraNearClip = mainCamera.nearClipPlane;
        }

        private void Update() 
        {
            if (Input.GetMouseButtonDown(0))
            {
                MouseDown?.Invoke();
            }
            else if (Input.GetMouseButtonUp(0))
            {
                MouseUp?.Invoke();
            }

            mousePosition = Input.mousePosition;
        }

        public Vector3 GetMousePosition()
        {
            mousePosition.z = cameraNearClip;
            return mousePosition;
        }

        public Vector3 GetMousePositionInWorld()
        {
            return mainCamera.ScreenToWorldPoint(GetMousePosition());
        }
    }
}

using FruitNinja.Fruits;
using FruitNinja.ObjectPool;
using UnityEngine;

namespace FruitNinja.Controllers
{
    public class Fruit : SpawnableFruit
    {
        public Quaternion GetRotation()
        {
            return rb.rotation;
        }

        public Vector3 GetVelocity()
        {
            return rb.velocity;
        }
    }
}

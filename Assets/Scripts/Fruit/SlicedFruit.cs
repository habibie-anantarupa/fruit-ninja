using System.Collections;
using System.Collections.Generic;
using FruitNinja.Controllers;
using FruitNinja.ObjectPool;
using UnityEngine;

namespace FruitNinja.Controllers
{
    public class SlicedFruit : Poolable
    {
        [SerializeField] private Rigidbody[] rigidbodies;

        private Vector3[] defaultLocalPositions;
        private Quaternion[] defaultLocalRotations;

        public override void Init(IPooler<Poolable> pooler)
        {
            base.Init(pooler);

            defaultLocalPositions = new Vector3[rigidbodies.Length];
            defaultLocalRotations = new Quaternion[rigidbodies.Length];

            for (int i = 0; i < rigidbodies.Length; i++)
            {
                Rigidbody rb = rigidbodies[i];
                defaultLocalPositions[i] = rb.transform.localPosition;
                defaultLocalRotations[i] = rb.transform.localRotation;
            }
        }

        public override void OnReclaim()
        {
            base.OnReclaim();

            ResetPieces();
        }

        private void ResetPieces()
        {
            for (int i = 0; i < rigidbodies.Length; i++)
            {
                Transform t = rigidbodies[i].transform;
                t.localPosition = defaultLocalPositions[i];
                t.localRotation = defaultLocalRotations[i];
            }
        }

        public void Slice(Vector3 velocity, float force)
        {
            foreach (Rigidbody rb in rigidbodies)
            {
                rb.velocity = velocity;
                rb.AddForce(-rb.transform.forward * force, ForceMode.Impulse);
            }

            Invoke("Deactivate", 3f);
        }

        public void Deactivate()
        {
            SelfReclaim();
        }
    }
}

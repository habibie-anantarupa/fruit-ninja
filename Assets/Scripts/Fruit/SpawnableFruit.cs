using FruitNinja.Controllers;
using FruitNinja.Fruits;
using FruitNinja.ObjectPool;
using UnityEngine;

namespace FruitNinja.Fruits
{
    public abstract class SpawnableFruit : Poolable, ISpawnableFruit
    {
        protected Rigidbody rb;
        protected IFruitSpawner spawner;

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
        }

        public void Launch(Vector3 direction, float force)
        {
            rb.AddForce(direction * force, ForceMode.Impulse);
        }

        public void SetSpawner(IFruitSpawner spawner)
        {
            this.spawner = spawner;
        }

        public void Slice()
        {
            (spawner as FruitSpawnerController).OnFruitSliced(this);
            spawner = null;
        }

        public override void OnReclaim()
        {
            base.OnReclaim();
            rb.velocity = Vector3.zero;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Floor") && transform.position.y >= other.transform.position.y)
            {
                (spawner as FruitSpawnerController).OnFruitHitFloor(this);
                spawner = null;
            }
        }
    }
}
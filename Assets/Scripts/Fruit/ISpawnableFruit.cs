
using UnityEngine;

namespace FruitNinja.Fruits
{
    public interface ISpawnableFruit : ISliceable
    {
        void SetSpawner(IFruitSpawner spawner);
        void Launch(Vector3 direction, float force);
    }
}
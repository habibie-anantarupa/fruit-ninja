using FruitNinja.Fruits;
using FruitNinja.ObjectPool;

namespace FruitNinja.Fruits
{
    public interface IFruitSpawner
    {
        ISpawnableFruit Spawn<T>() where T : ISpawnableFruit;
        void Despawn(ISpawnableFruit fruit);
    }
}
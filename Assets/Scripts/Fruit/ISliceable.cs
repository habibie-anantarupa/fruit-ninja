
namespace FruitNinja.Fruits
{
    public interface ISliceable
    {
        void Slice();
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja.UI
{
    public class MenuController : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;

        public void Show()
        {
            OnShow();
        }

        public void Hide()
        {
            OnHide();
        }

        protected virtual void OnShow()
        {
            if (canvas != null)
            {
                canvas.enabled = true;
            }
            else
            {
                gameObject.SetActive(true);
            }
        }

        protected virtual void OnHide()
        {
            if (canvas != null)
            {
                canvas.enabled = false;
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
    }
}

using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace FruitNinja.UI
{
    public class StartMenuController : MenuController
    {
        [SerializeField] private Button playButton, quitButton;

        public event Action PlayPressed;
        private void Awake() 
        {
            playButton.onClick.AddListener(Play);
            quitButton.onClick.AddListener(Quit);
        }

        private void OnDestroy() 
        {
            playButton.onClick.RemoveAllListeners();
            quitButton.onClick.RemoveAllListeners();
        }

        protected override void OnShow()
        {
            base.OnShow();
        }

        private void OnPlayPressed()
        {
            Hide();
            PlayPressed?.Invoke();
        }

        private void Play()
        {
            OnPlayPressed();
        }

        private void Quit()
        {
            EditorApplication.ExitPlaymode();
        }
    }
}

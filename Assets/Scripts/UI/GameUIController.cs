using System.Collections;
using System.Collections.Generic;
using FruitNinja.Game;
using TMPro;
using UnityEngine;

namespace FruitNinja.UI
{
    public class GameUIController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private TextMeshProUGUI fruitSlicedNumberText, fruitMissedNumberText, timerText, healthText;

        private void Awake() 
        {
            gameManager.FruitSliced += OnFruitSliced;
            gameManager.FruitMissed += OnFruitMissed;
            gameManager.TimeChanged += OnTimeChanged;
            gameManager.HealthChanged += OnHealthChanged;
        }

        private void OnDestroy() 
        {
            gameManager.FruitSliced -= OnFruitSliced;
            gameManager.FruitMissed -= OnFruitMissed;
            gameManager.TimeChanged -= OnTimeChanged;
            gameManager.HealthChanged -= OnHealthChanged;
        }

        private void OnFruitSliced(int count)
        {
            fruitSlicedNumberText.text = count.ToString();
        }

        private void OnFruitMissed(int count)
        {
            fruitMissedNumberText.text = count.ToString();
        }

        private void OnTimeChanged(float time)
        {
            timerText.text = Mathf.RoundToInt(time).ToString();
        }

        private void OnHealthChanged(int health)
        {
            healthText.text = health.ToString();
        }
    }
}

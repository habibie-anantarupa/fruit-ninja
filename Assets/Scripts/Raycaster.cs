using System.Collections;
using System.Collections.Generic;
using FruitNinja.Game;
using UnityEngine;
using UnityEngine.EventSystems;

namespace FruitNinja.Utils
{
    public class Raycaster
    {
        private Camera mainCamera;
        private InputController inputController;
        private Ray ray;
        private RaycastHit hitInfo;

        public Raycaster(Camera camera, InputController inputController) 
        {
            mainCamera = camera;
            this.inputController = inputController;

            ray = new Ray();
            hitInfo = new RaycastHit();
        }

        public RaycastHit RaycastToMousePosition(LayerMask targetLayer, bool debug = false)
        {
            Vector3 targetPosition = mainCamera.ScreenToWorldPoint(inputController.GetMousePosition());
            ray.origin = mainCamera.transform.position;
            ray.direction = (targetPosition - mainCamera.transform.position).normalized;

            if (debug)
            {
                Debug.DrawRay(ray.origin, ray.direction * float.MaxValue, Color.red);
            }

            if (Physics.Raycast(ray, out hitInfo, float.MaxValue, targetLayer) && !EventSystem.current.IsPointerOverGameObject())
            {
                return hitInfo;
            }

            return hitInfo;
        }
    }
}

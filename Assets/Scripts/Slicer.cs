using FruitNinja.Utils;
using FruitNinja.Fruits;
using UnityEngine;

namespace FruitNinja.Game
{
    public class Slicer : MonoBehaviour
    {
        [SerializeField] private InputController inputController;
        [SerializeField] private LayerMask fruitLayerMask;

        private const float MinimumSliceVelocity = 0.01f;
        private bool isSlicing, canSlice;
        private Raycaster raycaster;
        private TrailRenderer trailRenderer;
        private Collider fruit;


        private void Awake() 
        {
            trailRenderer = GetComponent<TrailRenderer>();

            inputController.MouseDown += OnInputMouseDown;
            inputController.MouseUp += OnInputMouseUp;

            raycaster = new Raycaster(Camera.main, inputController);
        }

        private void Update() 
        {
            Slicing();
        }

        private void OnDestroy() 
        {
            inputController.MouseDown -= OnInputMouseDown;
            inputController.MouseUp -= OnInputMouseUp;
        }

        public void Activate()
        {
            canSlice = true;
        }

        public void Deactivate()
        {
            canSlice = false;
            EndSlice();
        }

        private void BeginSlice()
        {
            isSlicing = true;
            trailRenderer.Clear();
            trailRenderer.enabled = true;
        }

        private void Slicing()
        {
            if (!isSlicing || !canSlice) return;
            Vector3 position = inputController.GetMousePositionInWorld();
            transform.position = position;
            
            RaycastHit hitInfo = raycaster.RaycastToMousePosition(fruitLayerMask, true);
            fruit = hitInfo.collider;

            if (fruit != null)
            {
                if (fruit.transform.parent.TryGetComponent<ISliceable>(out ISliceable sliceable))
                {
                    sliceable.Slice();
                    fruit = null;
                }
            }
        }

        private void EndSlice()
        {
            isSlicing = false;
            trailRenderer.enabled = false;
            trailRenderer.Clear();
        }

        private void OnInputMouseDown()
        {
            BeginSlice();
        }

        private void OnInputMouseUp()
        {
            EndSlice();
        }
    }
}
